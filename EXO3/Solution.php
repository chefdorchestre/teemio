<?php

class Solution
{
// la fonction qui verifie la validité d'une chaine $s
 function isValid($s)
 {
   // la longueur de la chaine
   $l = strlen($s);
   // on verifie la présence des parenthese au début et à la fin de la chaine
   if(($s[0] == "(" || $s[0] == ")") && ($s[$l-1] == "(" ||  $s[$l-1] == ")"))
   {
     return true;
   }else{
     return false;
   }
 }
  function minAddToMakeValid($s)
   {
    $l = strlen($s); // la longeur de la chaine
    $dParenthese = 0; // compte cette parenthese (
    $gParenthese = 0; // compte cette parenthese )
    //  si vide renvoi valide
    if($s == "")
    {
      return "valide";
    }
    //  si valide et composée par parenthese on les comptes
    if($this->isValid($s)){
      for($i = 0; $i< $l ; $i++){
        if($s[$i] == "("){
          $dParenthese++;
        }elseif(")"){
          $gParenthese++;
        }else{
          return "invalide";
        }
      }
      // on retourne le nombre de parentheses <manquantes
      $iCount = $dParenthese - $gParenthese;
      if($iCount>=0){
        return $iCount;
      }else{
        return $iCount*(-1);
      }

    }else{
      return "invalide";
    }
  }
  // function test()
  // {
  // }
}
// valeurs de test
// () ==> 0
// ())) ==> 2
// (((==> 3
// "" ==> valide
// "azerty" ==> invalide

$test = new Solution();
print_r($test->minAddToMakeValid("(azerty)"));
