<?php
class BasicTestCases
{
  //  la fonction qui retourne le nombre de fois
  //  j'utilise le prefix $a pour array et le $i pour entier et $o pour objet
  function findIt(array $aArr):int
  {
    // tableau sans redondance.
    $aUnique = array_unique($aArr);
    // déclaration du tableau associatif contenant l'entier en index et son nombre d'occurence.
    $aCount = [];

    foreach ($aUnique as $key => $iUnique) {
      $iCount = 0;
      foreach ($aArr as $key => $iArr) {
        if($iUnique == $iArr){
          $iCount++;
          $aCount[$iUnique] = $iCount;
        }
      }
    }
    // on retourne le 1er entier qui apparait n fois (n impaire)
    if($aCount[$iUnique]%2 == 0){
      return 0 ;
    }else{
      return $iUnique;
    }
  }
  // la fonction du test
  public function testFindIt()
  {
    return
      // 10 == $this->findIt([1,1,1,1,1,1,10,1,1,1,1]) ? true : false;
      // -1 == $this->findIt([1,1,2,-2,5,2,4,4,-1,-2,5]) ? true : false;
      // 5  == $this->findIt([20,1,1,2,2,3,3,5,5,4,20,4,5]) ? true : false;
      // 10  == $this->findIt([10]) ? true : false;
      10  == $this->findIt([1,1,1,1,1,1,10,1,1,1,1]) ? true : false;
  }
}
// appel de la fonction du test
$oTest = new BasicTestCases();
echo $oTest->testFindIt();
